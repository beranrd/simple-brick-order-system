package com.bamofah.brickstore.orderinfo;

import com.bamofah.brickstore.IntegrationTest;
import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql("/itDb/orderInfo/insert-controller-data.sql")
public class OrderInfoControllerIT extends IntegrationTest {

    @Test
    public void shouldReturnBadRequestWhenNoOrderFound() throws Exception {

        mockMvc.perform(get("/orders/14"))
               .andDo(print())
               .andExpect(status().isNotFound());
    }


    @Test
    public void shouldReturnOrderInfoWhenOrderFound() throws Exception {

        mockMvc.perform(get("/orders/10"))
               .andDo(print())
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.orderRef").value(10L))
               .andExpect(jsonPath("$.orderQty").value(15));
    }

    @Test
    public void shouldReturnAllOrders() throws Exception {

        mockMvc.perform(get("/orders"))
               .andDo(print())
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.length()").value(2))
               .andExpect(jsonPath("$[0].orderRef").value(2L))
               .andExpect(jsonPath("$[0].orderQty").value(1))
               .andExpect(jsonPath("$[1].orderRef").value(10L))
               .andExpect(jsonPath("$[1].orderQty").value(15));
    }


}