package com.bamofah.brickstore.orderinfo;

import com.bamofah.brickstore.IntegrationTest;
import com.bamofah.brickstore.common.exception.NotFoundException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@Sql("/itDb/orderInfo/insert-orderinfo-repo-data.sql")
public class OrderInfoRepoIT extends IntegrationTest {

    @Autowired
    private OrderInfoRepo orderInfoRepo;


    @Test(expected = NotFoundException.class)
    public void shouldReturnNotFoundExceptionWhenNoOrderRefFound() {

        OrderInfo orderInfo = orderInfoRepo.getOrderInfo(3L);
        assertNull("no order info found", orderInfo);
    }

    @Test
    public void shouldFindOrderInfoRecordForValidOrderRef() {

        OrderInfo orderInfo = orderInfoRepo.getOrderInfo(1L);
        assertNotEquals("order found for ref", orderInfo);
    }

    @Test
    public void shouldGetAllOrders() {

        List<OrderInfo> allOderInfo = orderInfoRepo.getOrders();
        assertEquals("2 orders found", 2, allOderInfo.size());
    }


}