package com.bamofah.brickstore.submit;

import com.bamofah.brickstore.IntegrationTest;
import com.bamofah.brickstore.common.exception.ConflictException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.jdbc.Sql;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertTrue;

@Sql("/itDb/submit/insert-submit-order-repo-data.sql")
public class SubmitRepoIT extends IntegrationTest {

    @Autowired
    private SubmitRepo submitRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    private final String CHECK_ORDER_STATUS = "select " +
        "CASE " +
        "   WHEN br.is_dispatched = 'Y' THEN 'true' " +
        "   ELSE 'false' " +
        "END " +
        "from bricks br where br.id = :orderRef;";

    @Test(expected = ConflictException.class)
    public void shouldThrowConflictExceptionWhenNoOrderWasUpdatedDuringSubmission() {

        submitRepo.submitOrder(1L);
    }

    @Test
    public void shouldSetOrderDispatchStatusToTrueDuringSubmission() {

        Long orderRef = 200L;
        Map<String, Object> param = Collections.singletonMap("orderRef", orderRef);
        submitRepo.submitOrder(orderRef);
        boolean isDispatched = namedParameterJdbcTemplate.queryForObject(CHECK_ORDER_STATUS, param, Boolean.class);

        assertTrue("order should be in despatched status", isDispatched);
    }
}