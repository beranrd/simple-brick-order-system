DROP TABLE bricks IF EXISTS;

CREATE TABLE bricks (
    id             BIGINT                              NOT NULL PRIMARY KEY IDENTITY,
    order_created  DATETIME                            NOT NULL,
    is_dispatched  VARCHAR(1) DEFAULT 'N'              NOT NULL,
    order_quantity INTEGER DEFAULT NULL
);