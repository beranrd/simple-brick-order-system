INSERT INTO bricks (id, order_created, order_quantity)
VALUES
    (1, now(), 15),
    (2, date_add(now(), INTERVAL 2 DAY), 1);