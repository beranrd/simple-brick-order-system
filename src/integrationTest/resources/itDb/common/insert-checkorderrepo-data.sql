INSERT INTO bricks (id, order_created, order_quantity)
VALUES
    (200, now(), 15);

INSERT INTO bricks (id, order_created, order_quantity, is_dispatched)
VALUES
    (210, now(), 5, 'Y');