package com.bamofah.brickstore.updateorder;

import com.bamofah.brickstore.common.exception.ConflictException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Repository
class UpdateOrderRepo {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateOrderRepo.class);

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String UPDATE_ORDER_QUERY = "update bricks " +
        "set order_quantity = :orderQty " +
        "where id = :orderRef; ";

    private final String CHECK_ORDER_STRING = "select count(*) from bricks where id = :orderRef; ";

    public void updateOrder(Long orderRef, int orderQty) {

        LOG.debug("updating order ref; {} with order quantity {} ;", orderRef, orderQty);

        Map<String, Object> params = new HashMap<>();
        params.put("orderRef", orderRef);
        params.put("orderQty", orderQty);
        int rowsAffected = namedParameterJdbcTemplate.update(UPDATE_ORDER_QUERY,params);

        if (rowsAffected <= 0) {
            LOG.warn("Failed to update order quantity for order ref {}", orderRef);
            throw new ConflictException("failed to update order : " + orderRef);
        }

    }

}
