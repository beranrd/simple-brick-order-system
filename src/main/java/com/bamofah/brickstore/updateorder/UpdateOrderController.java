package com.bamofah.brickstore.updateorder;

import com.bamofah.brickstore.common.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class UpdateOrderController {

    @Autowired
    private UpdateOrderService updateOrderService;

    @PutMapping("/orders")
    public ResponseEntity<Map<String, Long>> updateOrder(@RequestBody UpdateOrderBody orderBody) {

        validateRequestBody(orderBody);

        updateOrderService.updateOrder(orderBody.getOrderRef(), orderBody.getOrderQty());
        Map<String, Long> response = Collections.singletonMap("orderRef", orderBody.getOrderRef());

        return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
    }

    private void validateRequestBody(UpdateOrderBody orderBody) {
        if (orderBody == null) {
            throw new BadRequestException("Unable to peform order update - Update details need to be provided");
        }

        if (orderBody.getOrderRef() == null) {
            throw new BadRequestException("Unable to perform order update - Order ref needs to be provided");
        }

        if (orderBody.getOrderQty() <= 0) {
            throw new BadRequestException("Unable to perform order update - Order quantity must be greater than zeror");
        }
    }


}
