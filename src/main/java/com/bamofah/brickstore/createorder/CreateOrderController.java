package com.bamofah.brickstore.createorder;

import com.bamofah.brickstore.common.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class CreateOrderController {

    @Autowired
    private CreateOrderService createOrderService;

    @PostMapping("/orders/{orderQty}")
    public ResponseEntity<Map<String, Long>> createOrder(@PathVariable int orderQty) {

        validateOrderQty(orderQty);

        Long orderRef = createOrderService.createOrder(orderQty);
        Map<String, Long> response = Collections.singletonMap("orderRef", orderRef);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    private void validateOrderQty(int orderQty) {
        if (orderQty <= 0) {
            throw new BadRequestException("Order quanty should be greater than 0");
        }
    }
}
