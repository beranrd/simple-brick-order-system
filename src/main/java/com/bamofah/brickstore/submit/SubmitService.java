package com.bamofah.brickstore.submit;

import com.bamofah.brickstore.common.OrderCheckRepo;
import com.bamofah.brickstore.common.exception.BadRequestException;
import com.bamofah.brickstore.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class SubmitService {

    @Autowired
    public OrderCheckRepo orderCheckRepo;

    @Autowired
    private SubmitRepo submitRepo;

    public void submitOrder(Long orderRef) {

        if(!orderCheckRepo.orderExist(orderRef)){
            throw new NotFoundException("Unable to submit order - Order does not found");
        }

        submitRepo.submitOrder(orderRef);
    }
}
