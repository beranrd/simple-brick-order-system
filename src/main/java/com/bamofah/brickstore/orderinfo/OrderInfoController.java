package com.bamofah.brickstore.orderinfo;

import com.bamofah.brickstore.common.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderInfoController {

    @Autowired
    private OrderInfoService orderInfoService;

    @GetMapping("/orders/{orderRef}")
    public OrderInfo getOrderInfo(@PathVariable Long orderRef) {

        validateOrderRef(orderRef);
        return orderInfoService.getOrderInfo(orderRef);
    }

    @GetMapping("/orders")
    public List<OrderInfo> getAllOrders() {
        return orderInfoService.getAllOrders();
    }

    private void validateOrderRef(Long orderRef) {
        if (orderRef == null) {
            throw new BadRequestException("Unable to retrieve order ref - Please provide order ref");
        }
    }
}
