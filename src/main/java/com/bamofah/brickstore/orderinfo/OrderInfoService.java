package com.bamofah.brickstore.orderinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
class OrderInfoService {

    @Autowired
    private OrderInfoRepo orderInfoRepo;

    public OrderInfo getOrderInfo(Long orderRef) {
        return orderInfoRepo.getOrderInfo(orderRef);
    }

    public List<OrderInfo> getAllOrders() {
        return orderInfoRepo.getOrders();
    }
}
