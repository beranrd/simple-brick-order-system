package com.bamofah.brickstore.orderinfo;

import com.bamofah.brickstore.common.MyBaseRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderInfoRowMapper extends MyBaseRowMapper<OrderInfo> {

    @Override
    public OrderInfo mapRow(ResultSet rs, int rowNum) throws SQLException {

        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderRef(getLong(rs, "orderRef"));
        orderInfo.setOrderQty(getInteger(rs, "orderQty"));

        return orderInfo;
    }
}
