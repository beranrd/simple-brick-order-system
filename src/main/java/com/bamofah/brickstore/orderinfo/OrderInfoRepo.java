package com.bamofah.brickstore.orderinfo;

import com.bamofah.brickstore.common.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
class OrderInfoRepo {

    private static final Logger LOG = LoggerFactory.getLogger(OrderInfoRepo.class);

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final String GET_ORDER_INFO_QUERY = "select br.id as orderRef, " +
        "br.order_quantity as orderQty " +
        "FROM bricks br " +
        "where br.id = :orderRef; ";

    private static final String GET_ALL_ORDERS = "SELECT br.id AS orderRef, " +
        "br.order_quantity AS orderQty " +
        "FROM bricks br; ";

    public OrderInfo getOrderInfo(Long orderRef) {

        LOG.debug("Retrieving Order Info for order ref {}", orderRef);
        try {
            Map<String, Object> params = Collections.singletonMap("orderRef", orderRef);

            return namedParameterJdbcTemplate.queryForObject(GET_ORDER_INFO_QUERY, params, new OrderInfoRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn("No order info found for order ref {}", orderRef);
            throw new NotFoundException("Failed to find order ref " + orderRef, ex);
        }
    }

    public List<OrderInfo> getOrders() {

        LOG.debug("Getting all Orders");

        return namedParameterJdbcTemplate.query(GET_ALL_ORDERS, (SqlParameterSource) null, new OrderInfoRowMapper());
    }
}
