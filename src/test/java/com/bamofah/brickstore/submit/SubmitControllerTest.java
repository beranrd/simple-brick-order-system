package com.bamofah.brickstore.submit;

import com.bamofah.brickstore.common.exception.BadRequestException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class SubmitControllerTest {

    @InjectMocks
    private SubmitController controller;

    @Mock
    private SubmitService mockSubmitService;

    @Before
    public void setUp(){

        initMocks(this);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestExceptionWhenOrderRefIsInvalid(){

        controller.submitOrder(null);
    }

}