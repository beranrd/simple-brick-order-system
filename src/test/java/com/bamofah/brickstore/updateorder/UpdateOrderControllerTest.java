package com.bamofah.brickstore.updateorder;

import com.bamofah.brickstore.common.exception.BadRequestException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.MockitoAnnotations.initMocks;

public class UpdateOrderControllerTest {

    @InjectMocks
    private UpdateOrderController controller;

    @Mock
    private UpdateOrderService mockUpdateOrderService;

    private UpdateOrderBody orderBody;

    @Before
    public void setUp() {

        initMocks(this);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestExceptionWhenInvalidOrderRefDuringUpdate() {

        orderBody = new UpdateOrderBody();
        orderBody.setOrderQty(4);
        controller.updateOrder(orderBody);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestExceptionWhenZeroOrNegativeOrderQtyDuringUpdate() {

        orderBody = new UpdateOrderBody();
        orderBody.setOrderRef(1L);
        orderBody.setOrderQty(0);
        controller.updateOrder(orderBody);

        orderBody.setOrderQty(-10);
        controller.updateOrder(orderBody);
    }

    @Test(expected = BadRequestException.class)
    public void shouldThrowBadRequestExceptionWhenNoUpdateDetailsProvided() {

        controller.updateOrder(orderBody);
    }
}