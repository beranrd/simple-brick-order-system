package com.bamofah.brickstore.updateorder;

import com.bamofah.brickstore.common.OrderCheckRepo;
import com.bamofah.brickstore.common.exception.BadRequestException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class UpdateOrderServiceTest {

    @InjectMocks
    private UpdateOrderService service;

    @Mock
    private OrderCheckRepo mockCheckOrderRepo;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test(expected = BadRequestException.class)
    public void shouldReturnBadRequestWhenNoOrderFoundDrringUpdate() {

        when(mockCheckOrderRepo.orderExist(any(Long.class))).thenReturn(false);

        service.updateOrder(1L, 2);
    }
}